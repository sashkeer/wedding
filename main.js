const host = '';
const envelope = document.getElementById('envelope');
const wrapper = document.getElementById('wrapper');
const weddingDate = document.getElementById('weddingDate');

const currentUsers = [];
let glitchTimer;

let greetingMessage = '';
let checkMessage = 'Пожалуйста, если Вы придёте, нажмите на крестик в левом нижнем углу!';

const languages = {
  ru: {
    HUSBAND: 'Александр',
    WIFE: 'Евангелина',
    DATE: '6 ноября 2020',
    GREETING: 'Дорогие',
    REQUEST1: 'Приглашаем разделить с нами церемонию бракосочетания!',
    REQUEST2: 'Будем счастливы видеть на регистрации брака и торжестве, посвященном этому знаменательному событию.',
    CHECK_PLEASE: 'Пожалуйста, если Вы придёте, нажмите на крестик в левом нижнем углу!',
    COLOR_PALETTE: 'Цветовая палитра изумрудный и бежевый.',
    THANKS: 'Спасибо',
    CLICK_MESSAGE: 'Нажми если придешь',
    CEREMONY_STARTS: 'Церемония состоится <br> в',
    ADDRESS: 'в коттедже<br> по адресу',
    AND: 'и',
  },
  en: {
    HUSBAND: 'Aleksandr',
    WIFE: 'Evangelina',
    DATE: '6th of November 2020',
    GREETING: 'Dear',
    REQUEST1: 'We request the pleasure of your company at our marriage!',
    REQUEST2: 'Will be happy to see you at our wedding.',
    CHECK_PLEASE: 'Please, if you can visit us click the cross in bottom left corner',
    COLOR_PALETTE: 'Color palette',
    THANKS: 'Thanks',
    CLICK_MESSAGE: 'Click if you come',
    CEREMONY_STARTS: 'Starts at',
    ADDRESS: '<br> address',
    AND: 'and',
  }
};

let glitches = document.querySelectorAll('[glitch]');
const checkPlease = document.querySelector('#checkPlease');
const greeting = document.querySelector('#greeting');
const greetingUsers = document.querySelector('#greetingUsers');
const tabs = document.querySelectorAll('[invitation-tab]');
const toggle = document.querySelector('.toggleButton input');
const thanks = document.querySelector('.post-scriptum-thanks');
const noThanks = document.querySelector('.post-scriptum-no-thanks');
const weddingNames = document.querySelector('.wedding-names');
const errorNames = document.querySelector('.error-names');

const userIds = getQueryVariable('user');

if (!userIds) {
    weddingNames.classList.add('hidden');
    errorNames.classList.remove('hidden');
    wrapper.classList.add('error');
    weddingDate.innerText = `1 января 1970`;
}

setTimeout(() => {
    envelope.classList.add('flip');
}, 5000);

function getQueryVariable (variable) {
    let query = window.location.search.substring(1);
    let vars = query.split('&');
    for (let i = 0; i < vars.length; i++) {
        let pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) === variable) {
            return decodeURIComponent(pair[1]);
        }
    }
}

function guestWillCome (id) {
    fetch(`${host}/api/guest/willCome/${id}`, { method: 'get' }).then();
}

function guestWillNotCome (id) {
    fetch(`${host}/api/guest/willNotCome/${id}`, { method: 'get' }).then();
}

fetch(`${host}/api/guests/${userIds}`, { method: 'get' })
.then(response => response.json())
.then(jsonData => {
  const lang = getQueryVariable('lang') || 'ru';

    if (!jsonData || !jsonData.length) {
        weddingNames.classList.add('hidden');
        errorNames.classList.remove('hidden');
        wrapper.classList.add('error');
        weddingDate.innerText = `1 января 1970`;
    }

    if (jsonData && jsonData.length && lang === 'ru') {
        if (jsonData.length === 1) {
            if (jsonData[0].sex) {
              languages[lang].GREETING = 'Дорогой';

            } else {
              languages[lang].GREETING = 'Дорогая';
            }
            languages[lang].CHECK_PLEASE = 'Пожалуйста, если ты придешь, нажми на галочку в левом нижнем углу!';
        } else {
          languages[lang].GREETING = 'Дорогие';
        }
    }

    const users = [];
    let willCome = true;

    jsonData.forEach((user, index) => {
        users.push(
            `
            <span class="glitch"
                glitch
                default="%человек${index}"
                value="${user.alias}"
                data-text="%человек${index}">%человек${index}</span>
            `
        );

        willCome = willCome && user.willCome;
    });

    checkWillCome(willCome);

    try {
        greeting.innerHTML = languages[lang].GREETING;
        greetingUsers.innerHTML = users.join(` ${languages[lang].AND} `);
        checkPlease.innerHTML = languages[lang].CHECK_PLEASE;
    } catch (e) {
        console.log(e);
    }

    glitches = document.querySelectorAll('[glitch]');

    glitches.forEach((element) => {
        const value = element.getAttribute('value');
        glitchThis(element, value, 'glitch-finish');
    });
});

function saveGuest (event) {
    event.stopPropagation();
    event.preventDefault();

  checkWillCome(!toggle.checked);

    if (toggle.checked) {
        guestWillCome(userIds);
    } else {
        guestWillNotCome(userIds);
    }
}

function checkWillCome (check) {
    toggle.checked = check;

    if (toggle.checked) {
        thanks.classList.remove('transparent');
        noThanks.classList.add('transparent');
    } else {
        noThanks.classList.remove('transparent');
        thanks.classList.add('transparent');
    }
}

// const animate = setInterval(() => toggle.checked = !toggle.checked, 3000);
//
// document.querySelector('body').addEventListener('click', () => {
//     clearInterval(animate);
// })

function switchTo (event, target) {
    event.stopPropagation();
    event.preventDefault();

    tabs.forEach((element) => {
        const tabName = element.getAttribute('invitation-tab');
        if (tabName === target) {
            element.classList.remove('hidden');
        } else {
            element.classList.add('hidden');
        }

        if (target === 'map') {
            envelope.classList.add('map');
        } else {
            envelope.classList.remove('map');
        }
    });
}

function flipInvitation (event) {
    switchTo(event, 'invitation');
    if (envelope.classList.contains('flip')) {
        envelope.classList.remove('flip');

        glitches.forEach((element) => {
            const defaultValue = element.getAttribute('default');
            element.innerText = defaultValue;
            element.setAttribute('data-text', defaultValue);
            element.setAttribute('class', 'glitch');
        });

        clearTimeout(glitchTimer);
    } else {
        envelope.classList.add('flip');
        glitchTimer = setTimeout(() => {

            glitches.forEach((element) => {
                const value = element.getAttribute('value');
                glitchThis(element, value, 'glitch-finish');
            });
        }, 1500);
    }
}

function glitchThis (user, name, addClass) {
    user.innerText = name;
    user.setAttribute('data-text', name);
    user.setAttribute('class', addClass);
}

function translate () {
  const lang = getQueryVariable('lang') || 'ru';
  console.log('translate');
  Object.keys(languages[lang]).forEach((key) => {
      if (document.querySelector(`[lang="${key}"]`)) {
        document.querySelector(`[lang="${key}"]`).innerHTML = languages[lang][key];
      }
  });

}


function start () {
  translate();
}

start();