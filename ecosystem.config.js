module.exports = {
    apps : [{
        port: 80,
        name: 'wedding',
        script: 'serve',
        instances: 1,
        autorestart: true,
        watch: false,
        max_memory_restart: '1G',
    }],

    deploy : {
        production : {
            user : 'root',
            host : '104.248.37.36',
            ref  : 'origin/master',
            repo : 'git@gitlab.com:sashkeer/wedding.git',
            path : '/var/www/wedding',
            'post-deploy' : 'pm2 reload ecosystem.config.js --env production'
        }
    }
};
